﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tour_huaraz.Enums
{
    public enum TipoPersonaType
    {
        PERSONA_NATURAL = 1,
        PERSONA_JURIDICA = 2
    }
}
