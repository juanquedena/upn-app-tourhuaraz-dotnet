﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tour_huaraz.DTO
{
    public class DataTableDTO<E>
    {
        
        public String Draw { get; set; }

        public long RecordsTotal { get; set; }

        public long RecordsFiltered { get; set; }

        public IEnumerable<E> Data { get; set; }
    }
}
