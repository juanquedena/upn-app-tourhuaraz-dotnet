use tour_huaraz
go

drop procedure sp_crear_cliente;
go

create procedure sp_crear_cliente(
	@tipo numeric(18, 0),
	@params xml
)
as
begin
	set nocount on;

	declare @codigo int

	begin transaction;

	begin try

		merge persona as a
		using (
			select
				x.v.value('codigo[1]', 'int') codigo,
				x.v.value('nombres[1]', 'varchar(150)') nombres,
				x.v.value('apellido_paterno[1]', 'varchar(150)') apellido_paterno,
				x.v.value('apellido_materno[1]', 'varchar(150)') apellido_materno,
				x.v.value('tipo_documento[1]', 'int') tipo_documento,
				x.v.value('nro_documento[1]', 'varchar(20)') nro_documento,
				x.v.value('correo_electronico[1]', 'varchar(100)') correo_electronico,
				x.v.value('sexo[1]', 'bit') sexo,
				x.v.value('telefono_fijo[1]', 'varchar(20)') telefono_fijo,
				x.v.value('telefono_movil[1]', 'varchar(20)') telefono_movil,
				x.v.value('direccion[1]', 'varchar(150)') direccion
			from @params.nodes('/cliente/row') x(v)
		) as b
		on a.codigo=b.codigo -- Revisar si se usa tipo y nro de documento
		when matched then update set
			a.nombres=b.nombres,
			a.apellido_paterno=b.apellido_paterno,
			a.apellido_materno=b.apellido_materno,
			a.tipo_documento=b.tipo_documento,
			a.nro_documento=b.nro_documento,
			a.correo_electronico=b.correo_electronico,
			a.sexo=b.sexo,
			a.telefono_fijo=b.telefono_fijo,
			a.telefono_movil=b.telefono_movil,
			a.direccion=b.direccion
		when not matched then insert (
			nombres,
			apellido_paterno,
			apellido_materno,
			tipo_documento,
			nro_documento,
			correo_electronico,
			sexo,
			telefono_fijo,
			telefono_movil,
			direccion
		) values (
			b.nombres,
			b.apellido_paterno,
			b.apellido_materno,
			b.tipo_documento,
			b.nro_documento,
			b.correo_electronico,
			b.sexo,
			b.telefono_fijo,
			b.telefono_movil,
			b.direccion
		);
		set @codigo = ( select @@identity )
		if(@codigo is null)
			set @codigo = (
				select x.v.value('codigo[1]', 'int') codigo
				from @params.nodes('/cliente/row') x(v)
			)

		merge cliente as a
		using (
			select 
				@codigo codigo_persona,
				x.v.value('tipo[1]', 'varchar(25)') tipo,
				x.v.value('categoria[1]', 'varchar(100)') categoria
			from @params.nodes('/cliente/row') x(v)
		) as b
		on a.codigo_persona=b.codigo_persona
		when matched then update set
			a.tipo=b.tipo,
			a.categoria=b.categoria
		when not matched then insert (
			codigo_persona,
			tipo,
			categoria
		) values (
			b.codigo_persona,
			b.tipo,
			b.categoria
		);

		if @tipo = 1
			merge cliente_natural as a
			using (
				select 
					@codigo codigo_cliente,
					x.v.value('nacionalidad[1]', 'varchar(50)') nacionalidad,
					convert(date, x.v.value('fecha_nacimiento[1]', 'varchar(25)'), 103) fecha_nacimiento
				from @params.nodes('/cliente/row') x(v)
			) as b
			on a.codigo_cliente=b.codigo_cliente
			when matched then update set
				a.nacionalidad=b.nacionalidad,
				a.fecha_nacimiento=b.fecha_nacimiento
			when not matched then insert (
				codigo_cliente,
				nacionalidad,
				fecha_nacimiento
			) values (
				b.codigo_cliente,
				b.nacionalidad,
				b.fecha_nacimiento
			);
		else if @tipo = 2
			merge cliente_juridico as a
			using (
				select 
					@codigo codigo_cliente,
					x.v.value('razon_social[1]', 'varchar(250)') razon_social,
					x.v.value('codigo_persona_contacto[1]', 'int') codigo_persona_contacto
				from @params.nodes('/cliente/row') x(v)
			) as b
			on a.codigo_cliente=b.codigo_cliente
			when matched then update set
				a.razon_social=b.razon_social,
				a.codigo_persona_contacto=b.codigo_persona_contacto
			when not matched then insert (
				codigo_cliente,
				razon_social,
				codigo_persona_contacto
			) values (
				b.codigo_cliente,
				b.razon_social,
				b.codigo_persona_contacto
			);

		commit transaction

		select @codigo codigo
	end try
	begin catch
		declare @error int, @message varchar(4000)
		select @error = error_number(), @message = error_message()
		
		if @@trancount >= 0
            rollback transaction;

		raiserror ('sp_crear_cliente: %d: %s', 16, 1, @error, @message) ;
	end catch
end
go

exec sp_crear_cliente 1, '<cliente><row><codigo>0</codigo><nombres>jyq f</nombres><apellido_paterno>s</apellido_paterno><apellido_materno>s</apellido_materno><tipo_documento>1</tipo_documento><nro_documento>12345678</nro_documento><correo_electronico>d@d.com</correo_electronico><sexo>1</sexo><telefono_fijo>13</telefono_fijo><telefono_movil>3333</telefono_movil><direccion>d dsd sfsd</direccion></row></cliente>'


declare @xml xml;

set @xml = '
<turistas>
<cliente><row><codigo>0</codigo><nombres>jyq f</nombres><apellido_paterno>s</apellido_paterno><apellido_materno>s</apellido_materno><tipo_documento>1</tipo_documento><nro_documento>12345678</nro_documento><correo_electronico>d@d.com</correo_electronico><sexo>1</sexo><telefono_fijo>13</telefono_fijo><telefono_movil>3333</telefono_movil><direccion>d dsd sfsd</direccion></row></cliente>
<cliente><row><codigo>0</codigo><nombres>jyq f</nombres><apellido_paterno>s</apellido_paterno><apellido_materno>s</apellido_materno><tipo_documento>1</tipo_documento><nro_documento>12345678</nro_documento><correo_electronico>d@d.com</correo_electronico><sexo>1</sexo><telefono_fijo>13</telefono_fijo><telefono_movil>3333</telefono_movil><direccion>d dsd sfsd</direccion></row></cliente>
<cliente><row><codigo>0</codigo><nombres>jyq f</nombres><apellido_paterno>s</apellido_paterno><apellido_materno>s</apellido_materno><tipo_documento>1</tipo_documento><nro_documento>12345678</nro_documento><correo_electronico>d@d.com</correo_electronico><sexo>1</sexo><telefono_fijo>13</telefono_fijo><telefono_movil>3333</telefono_movil><direccion>d dsd sfsd</direccion></row></cliente>
</turistas>
'

select 
	x.v.query('.') cliente
from @xml.nodes('/turistas/cliente') x(v)


/*
create procedure [dbo].[usp_listar_age]
as
begin
	select * from agencia order by nombre asc
end
go

create procedure [dbo].[usp_listar_pers]
as
begin
	select * from persona order by nombres asc
end
go
*/

