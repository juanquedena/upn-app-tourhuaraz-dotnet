﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace tour_huaraz.Db
{
    public class Datasource
    {
        // private static readonly string connectionString = "Data Source=DESKTOP-U4E6C1H\\SQLEXPRESS;Initial Catalog=tour_huaraz;Persist Security Info=True;User ID=sa;Password=admin+2019";
        private static readonly string connectionString = "Server=tcp:xion.database.windows.net,1433;Initial Catalog = tour_huaraz; Persist Security Info=False;User ID =xion; Password=buchisapa@2019; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30;";

        public static SqlConnection Connection()
        {
            return new SqlConnection(connectionString);
        }

        private static SqlConnection OpenConnection()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            if (connection.State == ConnectionState.Closed)
                connection.Open();
            return connection;
        }

        private static SqlCommand CreateCommand(CommandType commandType, string sentence, Dictionary<string, object> parameters)
        {
            SqlCommand command = new SqlCommand
            {
                Connection = OpenConnection(),
                CommandText = sentence,
                CommandType = commandType
            };

            if (parameters != null && parameters.LongCount() > 0)
            {
                foreach (KeyValuePair<string, object> pair in parameters)
                {
                    command.Parameters.AddWithValue(pair.Key, pair.Value);
                }
            }

            return command;
        }

        public static DataTable ExecuteProcedure(string procedure, Dictionary<string, object> parameters)
        {
            DataTable table = new DataTable();
            SqlDataReader reader;
            SqlCommand command = CreateCommand(CommandType.StoredProcedure, procedure, parameters);

            reader = command.ExecuteReader();
            table.Load(reader);

            reader.Close();
            command.Connection.Close();

            return table;
        }

        public static DataTable ExecuteQuery(string sentence, Dictionary<string, object> parameters)
        {
            DataTable table = new DataTable();
            SqlDataReader reader;
            SqlCommand command = CreateCommand(CommandType.Text, sentence, parameters);

            reader = command.ExecuteReader();
            table.Load(reader);

            reader.Close();
            command.Connection.Close();

            return table;
        }

        public static void ExecuteUpdate(string procedure, Dictionary<string, object> parameters)
        {
            SqlCommand command = CreateCommand(CommandType.StoredProcedure, procedure, parameters);
            command.ExecuteNonQuery();
            command.Connection.Close();
        }
    }
}
