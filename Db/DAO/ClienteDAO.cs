﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.Models;
using tour_huaraz.DTO;
using tour_huaraz.Db.Wrapper;

namespace tour_huaraz.Db.DAO
{
    public class ClienteDAO
    {

        private static readonly string from = "from persona a " +
            "inner join cliente b on a.codigo=b.codigo_persona " +
            "left join cliente_natural c on b.codigo_persona=c.codigo_cliente " +
            "left join cliente_juridico d on b.codigo_persona=d.codigo_cliente ";

        private static readonly string select = "select " +
            "a.*, " +
            "case a.tipo_documento when 1 then 'DNI' else 'RUC' end tipo_documento_desc, " +
            "b.*, " +
            "c.nacionalidad, convert(varchar(25), c.fecha_nacimiento, 103) fecha_nacimiento, " +
            "d.razon_social, d.codigo_persona_contacto " + from;

        public static DataTableDTO<Cliente> List(String search, int start, int length, int draw)
        {
            string selectCount = "select count(1) cta " + from;
            string selectCountFilter = "select count(1) cta " + from;
            string selectWhere = select;
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (search != null)
            {
                parameters.Add("@search", search);
                string where = "where " +
                    "a.nro_documento=@search or " +
                    "a.nombres like '%' + @search + '%' or " +
                    "a.apellido_paterno like '%' + @search + '%' or " +
                    "a.apellido_materno like '%' + @search + '%' or " +
                    "d.razon_social like '%' + @search + '%' ";
                selectCountFilter += where;
                selectWhere += where;
            }
            selectWhere += $"order by codigo offset {start} rows fetch next {length} rows only";

            DataTable tableCount = Datasource.ExecuteQuery(selectCount, null);
            int rowCount = int.Parse(tableCount.Rows[0]["cta"].ToString());

            int rowCountFilter = rowCount;
            if (search != null)
            {
                DataTable tableCountFilter = Datasource.ExecuteQuery(selectCountFilter, parameters);
                rowCountFilter = int.Parse(tableCountFilter.Rows[0]["cta"].ToString());
            }

            DataTable table = Datasource.ExecuteQuery(selectWhere, parameters);
            List<Cliente> clientes = new List<Cliente>();
            foreach (DataRow row in table.Rows)
            {
                clientes.Add(new ClienteWrapper(row).Cliente);
            }
            DataTableDTO<Cliente> data = new DataTableDTO<Cliente>
            {
                Draw = draw.ToString(),
                Data = clientes,
                RecordsTotal = rowCount,
                RecordsFiltered = rowCountFilter
            };

            return data;
        }

        public static Cliente Obtain(int id)
        {
            Cliente cliente = null;
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@codigo", id }
            };
            DataTable table = Datasource.ExecuteQuery(select + " where a.codigo = @codigo", parameters);
            if(table.Rows.Count == 1)
            {
                cliente = new ClienteWrapper(table.Rows[0]).Cliente;
            }
            return cliente;
        }

        public static void SaveUpdate(ClienteJuridico cliente)
        {
            SaveUpdate(new ClienteWrapper(cliente));
        }

        public static void SaveUpdate(ClienteNatural cliente)
        {
            SaveUpdate(new ClienteWrapper(cliente));
        }

        private static void SaveUpdate(ClienteWrapper clienteWrapper)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@tipo", clienteWrapper.Cliente.Tipo },
                { "@params", clienteWrapper.ToXML }
            };
            Console.Out.Write(clienteWrapper.ToXML);
            DataTable table = Datasource.ExecuteProcedure("sp_crear_cliente", parameters);
            if (table.Rows.Count == 1)
            {
                clienteWrapper.Cliente.Codigo = int.Parse(table.Rows[0]["codigo"].ToString());
            }
        }

        public static void Delete(int id)
        {

        }
    }
}
