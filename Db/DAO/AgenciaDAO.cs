﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.DTO;
using tour_huaraz.Db.Wrapper;
using tour_huaraz.Models;

namespace tour_huaraz.Db.DAO
{
    public class AgenciaDAO
    {

        public static DataTableDTO<Agencia> List()
        {
            DataTable table = Datasource.ExecuteQuery("select * from agencia", null);
            List<Agencia> agencias = new List<Agencia>();
            foreach (DataRow row in table.Rows)
            {
                agencias.Add(new AgenciaWrapper(row).Agencia);
            }
            DataTableDTO<Agencia> data = new DataTableDTO<Agencia>
            {
                Draw = "1",
                Data = agencias,
                RecordsTotal = agencias.Count,
                RecordsFiltered = agencias.Count
            };

            return data;
        }
    }
}
