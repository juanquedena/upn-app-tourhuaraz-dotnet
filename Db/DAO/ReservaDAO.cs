﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.Models;
using tour_huaraz.DTO;
using tour_huaraz.Db.Wrapper;

namespace tour_huaraz.Db.DAO
{
    public class ReservaDAO
    {

        private static readonly string from = "from reserva a " +
            "inner join detalle_reserva b on a.codigo=b.codigo_reserva " +
            "inner join persona a1 on a.codigo_cliente=a1.codigo " +
            "inner join cliente b1 on a1.codigo=b1.codigo_persona " +
            "left join cliente_natural c1 on b1.codigo_persona=c1.codigo_cliente " +
            "left join cliente_juridico d1 on b1.codigo_persona=d1.codigo_cliente ";

        private static readonly string select = "select a.codigo codigo_reserva, a.codigo_agencia, a.fecha_reservacion, " +
            "b.cantidad_a, b.cantidad_b, b.cantidad_c, b.codigo_paquete, b.nombre_paquete, " +
            "a1.*, " +
            "case a1.tipo_documento when 1 then 'DNI' else 'RUC' end tipo_documento_desc, " +
            "b1.*, " +
            "c1.nacionalidad, convert(varchar(25), c1.fecha_nacimiento, 103) fecha_nacimiento, " +
            "d1.razon_social, d1.codigo_persona_contacto " + from;

        public static DataTableDTO<Reserva> List(String search, int start, int length, int draw)
        {
            string selectCount = "select count(1) cta " + from;
            string selectCountFilter = "select count(1) cta " + from;
            string selectWhere = select;
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (search != null)
            {
                parameters.Add("@search", search);
                string where = "where " +
                    "a.nro_documento=@search or " +
                    "a.nombres like '%' + @search + '%' or " +
                    "a.apellido_paterno like '%' + @search + '%' or " +
                    "a.apellido_materno like '%' + @search + '%' or " +
                    "d.razon_social like '%' + @search + '%' ";
                selectCountFilter += where;
                selectWhere += where;
            }
            selectWhere += $"order by codigo offset {start} rows fetch next {length} rows only";

            DataTable tableCount = Datasource.ExecuteQuery(selectCount, null);
            int rowCount = int.Parse(tableCount.Rows[0]["cta"].ToString());

            int rowCountFilter = rowCount;
            if (search != null)
            {
                DataTable tableCountFilter = Datasource.ExecuteQuery(selectCountFilter, parameters);
                rowCountFilter = int.Parse(tableCountFilter.Rows[0]["cta"].ToString());
            }

            DataTable table = Datasource.ExecuteQuery(selectWhere, parameters);
            List<Reserva> reservas = new List<Reserva>();
            foreach (DataRow row in table.Rows)
            {
                reservas.Add(new ReservaWrapper(row).Reserva);
            }
            DataTableDTO<Reserva> data = new DataTableDTO<Reserva>
            {
                Draw = draw.ToString(),
                Data = reservas,
                RecordsTotal = rowCount,
                RecordsFiltered = rowCountFilter
            };

            return data;
        }

        public static Reserva Obtain(int id)
        {
            Reserva reserva = null;
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@codigo", id }
            };
            DataTable table = Datasource.ExecuteQuery(select + " where a.codigo = @codigo", parameters);
            if(table.Rows.Count == 1)
            {
                reserva = new ReservaWrapper(table.Rows[0]).Reserva;
            }
            return reserva;
        }

        public static void Save(ReservaWrapper reservaWrapper)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "@params", reservaWrapper.ToXML }
            };
            Console.Out.Write(reservaWrapper.ToXML);
            DataTable table = Datasource.ExecuteProcedure("sp_crear_reserva", parameters);
            if (table.Rows.Count == 1)
            {
                reservaWrapper.Reserva.Codigo = int.Parse(table.Rows[0]["codigo"].ToString());
            }
        }

        public static void Delete(int id)
        {

        }
    }
}
