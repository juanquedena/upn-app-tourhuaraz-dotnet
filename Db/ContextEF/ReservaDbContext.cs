﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tour_huaraz.Models;

namespace tour_huaraz.Db.DbContextEF
{
    public class ReservaDbContext : DbContext
    {
        public ReservaDbContext(DbContextOptions<ReservaDbContext> options) : base(options)
        {
        }

        public DbSet<Persona> Personas { get; set; }
    }
}
