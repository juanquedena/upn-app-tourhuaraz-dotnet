﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.Models;
using tour_huaraz.Enums;

namespace tour_huaraz.Db.Wrapper
{
    public class ClienteWrapper : AbstractWrapper
    {

        public Cliente Cliente { get; }
        public String ToXML { get;  }

        public ClienteWrapper(DataRow dataRow)
        {
            Row = dataRow;
            if (FieldInt("tipo") == (int)TipoPersonaType.PERSONA_NATURAL)
            {
                ClienteNatural clienteNatural = new ClienteNatural
                {
                    Nombres = FieldString("nombres"),
                    ApellidoPaterno = FieldString("apellido_paterno"),
                    ApellidoMaterno = FieldString("apellido_materno"),
                    CorreoElectronico = FieldString("correo_electronico"),
                    Sexo = FieldBool("sexo"),
                    Nacionalidad = FieldString("nacionalidad"),
                    FechaNacimiento = FieldString("fecha_nacimiento")
                };
                clienteNatural.NombreCompleto = $"{clienteNatural.ApellidoPaterno} {clienteNatural.ApellidoMaterno}, {clienteNatural.Nombres}";
                Cliente = clienteNatural;
            } else
            {
                ClienteJuridico clienteJuridico = new ClienteJuridico
                {
                    RazonSocial = FieldString("razon_social"),
                    Contacto = FieldInt("codigo_persona_contacto"),
                    NombreCompleto = FieldString("razon_social")
                };
                Cliente = clienteJuridico;
            }
            Cliente.Codigo = FieldInt("codigo");
            Cliente.Tipo = FieldInt("tipo");
            Cliente.TipoDocumento = FieldInt("tipo_documento");
            Cliente.TipoDocumentoDescripcion = FieldString("tipo_documento_desc");
            Cliente.NroDocumento = FieldString("nro_documento");
            Cliente.Categoria = FieldString("categoria");
            Cliente.TelefonoFijo = FieldString("telefono_fijo");
            Cliente.TelefonoMovil = FieldString("telefono_movil");
            Cliente.Direccion = FieldString("direccion");
        }

        public ClienteWrapper(Cliente cliente)
        {
            ClienteNatural clienteNatural = new ClienteNatural();
            ClienteJuridico clienteJuridico = new ClienteJuridico();
            int sexo = 0;
            Cliente = cliente;
            if(cliente.Tipo == (int) TipoPersonaType.PERSONA_NATURAL)
            {
                clienteNatural = (ClienteNatural)cliente;
                sexo = clienteNatural.Sexo ? 1 : 0;
            } else
            {
                clienteJuridico = (ClienteJuridico)cliente;
            }
            ToXML = $"<cliente><row>" +
                $"<codigo>{cliente.Codigo}</codigo>" +
                $"<tipo>{cliente.Tipo}</tipo>" +
                $"<categoria>{cliente.Categoria}</categoria>" +
                $"<nombres>{clienteNatural.Nombres}</nombres>" +
                $"<apellido_paterno>{clienteNatural.ApellidoPaterno}</apellido_paterno>" +
                $"<apellido_materno>{clienteNatural.ApellidoMaterno}</apellido_materno>" +
                $"<tipo_documento>{cliente.TipoDocumento}</tipo_documento>" +
                $"<nro_documento>{cliente.NroDocumento}</nro_documento>" +
                $"<correo_electronico>{clienteNatural.CorreoElectronico}</correo_electronico>" +
                $"<sexo>{sexo}</sexo>" +
                $"<telefono_fijo>{cliente.TelefonoFijo}</telefono_fijo>" +
                $"<telefono_movil>{cliente.TelefonoMovil}</telefono_movil>" +
                $"<direccion>{cliente.Direccion}</direccion>" +
                $"<razon_social>{clienteJuridico.RazonSocial}</razon_social>" +
                $"<codigo_persona_contacto>{clienteJuridico.Contacto}</codigo_persona_contacto>" +
                $"</row></cliente>";
        }
    }
}
