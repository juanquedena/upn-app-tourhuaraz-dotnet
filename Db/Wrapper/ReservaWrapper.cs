﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.Models;
using tour_huaraz.Enums;

namespace tour_huaraz.Db.Wrapper
{
    public class ReservaWrapper : AbstractWrapper
    {

        public Reserva Reserva { get; }
        public String ToXML { get;  }

        public ReservaWrapper(DataRow dataRow)
        {
            Row = dataRow;
            Reserva= new Reserva {
                Cliente = new ClienteWrapper(dataRow).Cliente,
                Codigo = FieldInt("codigo_reserva"),
                CodigoAgencia = FieldInt("codigo_agencia"),
                FechaReservacion = FieldString("fecha_reservacion"),
                DetalleReserva = new DetalleReserva {
                    CodigoPaquete = FieldInt("codigo_paquete"),
                    NombrePaquete = FieldString("nombre_paquete"),
                    CantidadA = FieldInt("cantidad_a"),
                    CantidadB = FieldInt("cantidad_b"),
                    CantidadC = FieldInt("cantidad_c")
                }
            };
        }

        public ReservaWrapper(Reserva reserva)
        {
            Reserva = reserva;
            String detallesReserva = "";
            foreach (DetalleReserva row in reserva.DetallesReserva)
            {
                String detalle = $"<row>" +
                    $"<codigo_paquete>{row.CodigoPaquete}</codigo_paquete>" +
                    $"<cantidad_a>{row.CantidadA}</cantidad_a>" +
                    $"<cantidad_b>{row.CantidadB}</cantidad_b>" +
                    $"<cantidad_c>{row.CantidadC}</cantidad_c>" +
                    $"<precio_a>{row.PrecioA}</precio_a>" +
                    $"<precio_b>{row.PrecioB}</precio_b>" +
                    $"<precio_c>{row.PrecioC}</precio_c>" +
                    $"</row>";
                detallesReserva += detalle;
            }

            String turistas = "";
            foreach (Turista row in reserva.Turistas)
            {
                turistas += new ClienteWrapper(row).ToXML;
            }

            ToXML = $"<reserva>" +
                $"<codigo_cliente>{reserva.CodigoCliente}</codigo_cliente>" +
                $"<fecha_reservacion>{reserva.FechaReservacion}</fecha_reservacion>" +
                $"<estado>{reserva.Estado}</estado>" +
                $"<codigo_agencia>{reserva.CodigoAgencia}</codigo_agencia>" +
                $"<subtotal>{reserva.SubTotal}</subtotal>" +
                $"<igv>{reserva.IGV}</igv>" +
                $"<total>{reserva.Total}</total>" +
                $"</reserva>" +
                $"<detalle_reserva>{detallesReserva}</detalle_reserva>";
            
            if(turistas.Length > 0) {
                ToXML += $"<turistas>{turistas}</turistas>";
            }
                
        }
    }
}
