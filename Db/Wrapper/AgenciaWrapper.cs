﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using tour_huaraz.Models;
using tour_huaraz.Enums;

namespace tour_huaraz.Db.Wrapper
{
    public class AgenciaWrapper : AbstractWrapper
    {

        public Agencia Agencia { get; }

        public AgenciaWrapper(DataRow dataRow)
        {
            Row = dataRow;
            Agencia = new Agencia
            {
                Codigo = FieldInt("codigo"),
                Nombre = FieldString("nombre"),
                Descripcion = FieldString("descripcion")
            };
        }

    }
}
