﻿using System;
using System.ComponentModel.DataAnnotations;

namespace tour_huaraz.Models
{

    public class ClienteNatural : Cliente
    {

        [Required(ErrorMessage = "Los nombres son obligatorios")]
        public String Nombres { get; set; }

        [Required(ErrorMessage = "El apellido paterno es obligatorio")]
        public String ApellidoPaterno { get; set; }

        [Required(ErrorMessage = "El apellido materno es obligatorio")]
        public String ApellidoMaterno { get; set; }

        [Required(ErrorMessage = "El correo electrónico es obligatorio")]
        public String CorreoElectronico { get; set; }

        [Required(ErrorMessage = "El sexo es obligatorio")]
        public Boolean Sexo { get; set; }

        public String Nacionalidad { get; set; }

        //  [RegularExpression("^((?!^Last Name$)[a-zA-Z '])+$", ErrorMessage = "Last name is required and must be properly formatted.")]
        public String FechaNacimiento { get; set; }

    }
}
