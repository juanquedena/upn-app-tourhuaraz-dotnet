﻿using System;
using System.ComponentModel.DataAnnotations;

namespace tour_huaraz.Models
{

    public class ClienteJuridico : Cliente
    {

        [Required(ErrorMessage = "La razón social es obligatoria")]
        public String RazonSocial { get; set; }

        public int? Contacto { get; set; }

    }
}
