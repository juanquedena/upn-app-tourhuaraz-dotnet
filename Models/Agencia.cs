﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tour_huaraz.Models
{
    public class Agencia
    {
        public int? Codigo { get; set; }

        public String Nombre { get; set; }

        public String Descripcion { get; set; }
    }
}
