using System;
using System.ComponentModel.DataAnnotations;

namespace tour_huaraz.Models
{

    public class DetalleReserva
    {
        public int? Codigo { get; set; }

	    [Required(ErrorMessage = "El c�digo de paquete es obligatoria")]
	    public int? CodigoPaquete{ get; set; }
	
		[Required(ErrorMessage = "El nombre de paquete es obligatoria")]
		public String NombrePaquete{ get; set; }

	    [Required(ErrorMessage = "La cantidad A es obligatoria")]
	    public int? CantidadA{ get; set; }
	
	    [Required(ErrorMessage = "La cantidad B es obligatoria")]
	    public int? CantidadB{ get; set; }
	
	    [Required(ErrorMessage = "La cantidad C es obligatoria")]
	    public int? CantidadC{ get; set; }

        public double? PrecioA { get; set; }

        public double? PrecioB { get; set; }

        public double? PrecioC { get; set; }
    }
}
