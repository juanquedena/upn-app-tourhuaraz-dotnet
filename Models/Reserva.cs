using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace tour_huaraz.Models
{

    public class Reserva
    {
        public int? Codigo { get; set; }
	
	    [Required(ErrorMessage = "El c�digo de cliente es obligatorio")]
        public int? CodigoCliente{ get; set; }

        public Cliente Cliente{ get; set; }
	
	    [Required(ErrorMessage = "El c�digo de agencia es obligatorio")]
	    public int? CodigoAgencia{ get; set; }
	
	    [Required(ErrorMessage = "La fecha de reservaci�n es obligatoria")]
	    public String FechaReservacion{ get; set; }
	
	    public int? Estado{ get; set; }

        public double? SubTotal { get; set; }

        public double? IGV { get; set; }

        public double? Total { get; set; }

        public List<DetalleReserva> DetallesReserva { get; set; }

        public DetalleReserva DetalleReserva { get; set; }

        public List<Turista> Turistas { get; set; }
    }
}
