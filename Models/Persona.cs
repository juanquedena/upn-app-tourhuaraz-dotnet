﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tour_huaraz.Models
{
    [Table("persona")]
    public class Persona
    {
        [Key, Column("codigo")]
        public int Codigo { get; set; }

        [Column("nombres")]
        [Required(ErrorMessage = "Los nombres son obligatorios")]
        public String Nombres { get; set; }

        [Column("apellido_paterno")]
        [Required(ErrorMessage = "El apellido paterno es obligatorio")]
        public String ApellidoPaterno { get; set; }

        [Column("apellido_materno")]
        [Required(ErrorMessage = "El apellido materno es obligatorio")]
        public String ApellidoMaterno { get; set; }

        [Column("tipo_documento")]
        [Required(ErrorMessage = "El tipo de documento es obligatorio")]
        public int TipoDocumento { get; set; }

        [Column("nro_documento")]
        [Required(ErrorMessage = "El nro. de documento es obligatorio")]
        public String NroDocumento { get; set; }

        [Column("correo_electronico")]
        [Required(ErrorMessage = "El correo electrónico es obligatorio")]
        public String CorreoElectronico { get; set; }

        [Column("sexo")]
        [Required(ErrorMessage = "El sexo es obligatorio")]
        public Boolean Sexo { get; set; }

        [Column("telefono_fijo")]
        [Required(ErrorMessage = "El teléfono fijo es obligatorio")]
        public String TelefonoFijo { get; set; }

        [Column("telefono_movil")]
        [Required(ErrorMessage = "El teléfono movil es obligatorio")]
        public String TelefonoMovil { get; set; }

        [Column("direccion")]
        [Required(ErrorMessage = "La dirección es obligatoria")]
        public String Direccion { get; set; }
    }
}
