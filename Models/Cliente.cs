﻿using System;
using System.ComponentModel.DataAnnotations;

namespace tour_huaraz.Models
{

    public class Cliente
    {
        public int? Codigo { get; set; }

        public int? Tipo { get; set; }

        [Required(ErrorMessage = "El tipo de documento es obligatorio")]
        public int? TipoDocumento { get; set; }

        public String TipoDocumentoDescripcion { get; set; }

        [Required(ErrorMessage = "El nro. de documento es obligatorio")]
        [MinLength(8, ErrorMessage = "El nro. de documento debe tener como mínimo 8 carácteres")]
        [MaxLength(11, ErrorMessage = "El nro. de documento debe tener como máximo 11 carácteres")]
        public String NroDocumento { get; set; }

        public String Categoria { get; set; }

        [RegularExpression("^(\\d{3}-)?(\\d{7})?$", ErrorMessage = "El télefono movil debe tener el siguiente formato 999-9999999 o 9999999")]
        public String TelefonoFijo { get; set; }

        [RegularExpression("^(\\d{3}-)?(\\d{9})?$", ErrorMessage = "El télefono movil debe tener el siguiente formato 999-999999999 o 999999999")]
        public String TelefonoMovil { get; set; }

        public String Direccion { get; set; }

        public String NombreCompleto { get; set; }
    }
}
