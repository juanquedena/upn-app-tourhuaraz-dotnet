USE [tour_huaraz]
GO
/****** Object:  Table [dbo].[agencia]    Script Date: 18/05/2019 10:09:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[agencia](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](250) NOT NULL,
	[descripcion] [varchar](250) NULL,
 CONSTRAINT [agencia_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cliente]    Script Date: 18/05/2019 10:09:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente](
	[codigo_persona] [int] NOT NULL,
	[tipo] [varchar](25) NOT NULL,
	[categoria] [varchar](25) NULL,
 CONSTRAINT [cliente_pk] PRIMARY KEY CLUSTERED 
(
	[codigo_persona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cliente_juridico]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente_juridico](
	[codigo_cliente] [int] NOT NULL,
	[razon_social] [varchar](250) NOT NULL,
	[codigo_persona_contacto] [int] NULL,
 CONSTRAINT [cliente_juridico_pk] PRIMARY KEY CLUSTERED 
(
	[codigo_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cliente_natural]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cliente_natural](
	[codigo_cliente] [int] NOT NULL,
	[nacionalidad] [varchar](50) NULL,
	[fecha_nacimiento] [date] NULL,
 CONSTRAINT [cliente_natural_pk] PRIMARY KEY CLUSTERED 
(
	[codigo_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalle_paquete]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_paquete](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[codigo_paquete] [int] NULL,
	[codigo_lugar] [int] NULL,
	[estado] [varchar](25) NULL,
 CONSTRAINT [PK_detalle_paquete] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalle_reserva]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalle_reserva](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[codigo_reserva] [int] NOT NULL,
	[codigo_paquete] [int] NOT NULL,
	[cantidad_a] [int] NOT NULL,
	[cantidad_b] [int] NOT NULL,
	[cantidad_c] [int] NOT NULL,
 CONSTRAINT [paquete_vendido_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[empleado]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empleado](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[codigo_persona] [int] NOT NULL,
	[codigo_agencia] [int] NOT NULL,
	[tipo_empleado] [varchar](25) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [guia_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lugar]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lugar](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](250) NOT NULL,
	[tipo] [varchar](25) NOT NULL,
 CONSTRAINT [lugar_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[paquete]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paquete](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[tarifa_a] [decimal](18, 3) NOT NULL,
	[tarifa_b] [decimal](18, 3) NOT NULL,
	[tarifa_c] [decimal](18, 3) NOT NULL,
	[estado] [varchar](25) NOT NULL,
 CONSTRAINT [paquete_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[persona]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[persona](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[nombres] [varchar](150) NULL,
	[apellido_paterno] [varchar](150) NULL,
	[apellido_materno] [varchar](150) NULL,
	[tipo_documento] [int] NOT NULL,
	[nro_documento] [varchar](20) NOT NULL,
	[correo_electronico] [varchar](150) NOT NULL,
	[sexo] [bit] NULL,
	[telefono_fijo] [varchar](20) NULL,
	[telefono_movil] [varchar](20) NULL,
	[direccion] [varchar](150) NULL,
 CONSTRAINT [persona_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reserva]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reserva](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[codigo_cliente] [int] NOT NULL,
	[fecha_reservacion] [datetime] NOT NULL,
	[estado] [tinyint] NOT NULL,
	[codigo_agencia] [int] NULL,
 CONSTRAINT [venta_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[turista]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[turista](
	[codigo] [int] IDENTITY(1,1) NOT NULL,
	[codigo_cliente_natural] [int] NOT NULL,
	[codigo_reserva] [int] NOT NULL,
 CONSTRAINT [turista_pk] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cliente]  WITH CHECK ADD  CONSTRAINT [Table_5_Persona] FOREIGN KEY([codigo_persona])
REFERENCES [dbo].[persona] ([codigo])
GO
ALTER TABLE [dbo].[cliente] CHECK CONSTRAINT [Table_5_Persona]
GO
ALTER TABLE [dbo].[cliente_juridico]  WITH CHECK ADD  CONSTRAINT [Cliente_Juridico_Cliente] FOREIGN KEY([codigo_cliente])
REFERENCES [dbo].[cliente] ([codigo_persona])
GO
ALTER TABLE [dbo].[cliente_juridico] CHECK CONSTRAINT [Cliente_Juridico_Cliente]
GO
ALTER TABLE [dbo].[cliente_juridico]  WITH NOCHECK ADD  CONSTRAINT [FK_cliente_juridico_persona] FOREIGN KEY([codigo_persona_contacto])
REFERENCES [dbo].[persona] ([codigo])
GO
ALTER TABLE [dbo].[cliente_juridico] NOCHECK CONSTRAINT [FK_cliente_juridico_persona]
GO
ALTER TABLE [dbo].[cliente_natural]  WITH CHECK ADD  CONSTRAINT [Cliente_Natural_Cliente] FOREIGN KEY([codigo_cliente])
REFERENCES [dbo].[cliente] ([codigo_persona])
GO
ALTER TABLE [dbo].[cliente_natural] CHECK CONSTRAINT [Cliente_Natural_Cliente]
GO
ALTER TABLE [dbo].[detalle_paquete]  WITH CHECK ADD  CONSTRAINT [FK_detalle_paquete_lugar] FOREIGN KEY([codigo_lugar])
REFERENCES [dbo].[lugar] ([codigo])
GO
ALTER TABLE [dbo].[detalle_paquete] CHECK CONSTRAINT [FK_detalle_paquete_lugar]
GO
ALTER TABLE [dbo].[detalle_paquete]  WITH CHECK ADD  CONSTRAINT [FK_detalle_paquete_paquete] FOREIGN KEY([codigo_paquete])
REFERENCES [dbo].[paquete] ([codigo])
GO
ALTER TABLE [dbo].[detalle_paquete] CHECK CONSTRAINT [FK_detalle_paquete_paquete]
GO
ALTER TABLE [dbo].[detalle_reserva]  WITH CHECK ADD  CONSTRAINT [Table_11_venta] FOREIGN KEY([codigo_reserva])
REFERENCES [dbo].[reserva] ([codigo])
GO
ALTER TABLE [dbo].[detalle_reserva] CHECK CONSTRAINT [Table_11_venta]
GO
ALTER TABLE [dbo].[empleado]  WITH CHECK ADD  CONSTRAINT [guia_agencia] FOREIGN KEY([codigo_agencia])
REFERENCES [dbo].[agencia] ([codigo])
GO
ALTER TABLE [dbo].[empleado] CHECK CONSTRAINT [guia_agencia]
GO
ALTER TABLE [dbo].[empleado]  WITH CHECK ADD  CONSTRAINT [guia_persona] FOREIGN KEY([codigo_persona])
REFERENCES [dbo].[persona] ([codigo])
GO
ALTER TABLE [dbo].[empleado] CHECK CONSTRAINT [guia_persona]
GO
ALTER TABLE [dbo].[reserva]  WITH CHECK ADD  CONSTRAINT [FK_reserva_agencia] FOREIGN KEY([codigo_agencia])
REFERENCES [dbo].[agencia] ([codigo])
GO
ALTER TABLE [dbo].[reserva] CHECK CONSTRAINT [FK_reserva_agencia]
GO
ALTER TABLE [dbo].[reserva]  WITH CHECK ADD  CONSTRAINT [venta_cliente] FOREIGN KEY([codigo_cliente])
REFERENCES [dbo].[cliente] ([codigo_persona])
GO
ALTER TABLE [dbo].[reserva] CHECK CONSTRAINT [venta_cliente]
GO
ALTER TABLE [dbo].[turista]  WITH CHECK ADD  CONSTRAINT [turista_cliente_natural] FOREIGN KEY([codigo_cliente_natural])
REFERENCES [dbo].[cliente_natural] ([codigo_cliente])
GO
ALTER TABLE [dbo].[turista] CHECK CONSTRAINT [turista_cliente_natural]
GO
ALTER TABLE [dbo].[turista]  WITH CHECK ADD  CONSTRAINT [turista_venta] FOREIGN KEY([codigo_reserva])
REFERENCES [dbo].[reserva] ([codigo])
GO
ALTER TABLE [dbo].[turista] CHECK CONSTRAINT [turista_venta]
GO
/****** Object:  StoredProcedure [dbo].[sp_crear_cliente]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_crear_cliente](
	@tipo numeric(18, 0),
	@params xml
)
as
begin
	set nocount on;

	declare @codigo int

	begin transaction;

	begin try

		merge persona as a
		using (
			select
				x.v.value('codigo[1]', 'int') codigo,
				x.v.value('nombres[1]', 'varchar(150)') nombres,
				x.v.value('apellido_paterno[1]', 'varchar(150)') apellido_paterno,
				x.v.value('apellido_materno[1]', 'varchar(150)') apellido_materno,
				x.v.value('tipo_documento[1]', 'int') tipo_documento,
				x.v.value('nro_documento[1]', 'varchar(20)') nro_documento,
				x.v.value('correo_electronico[1]', 'varchar(100)') correo_electronico,
				x.v.value('sexo[1]', 'bit') sexo,
				x.v.value('telefono_fijo[1]', 'varchar(20)') telefono_fijo,
				x.v.value('telefono_movil[1]', 'varchar(20)') telefono_movil,
				x.v.value('direccion[1]', 'varchar(150)') direccion
			from @params.nodes('/cliente/row') x(v)
		) as b
		on a.codigo=b.codigo -- Revisar si se usa tipo y nro de documento
		when matched then update set
			a.nombres=b.nombres,
			a.apellido_paterno=b.apellido_paterno,
			a.apellido_materno=b.apellido_materno,
			a.tipo_documento=b.tipo_documento,
			a.nro_documento=b.nro_documento,
			a.correo_electronico=b.correo_electronico,
			a.sexo=b.sexo,
			a.telefono_fijo=b.telefono_fijo,
			a.telefono_movil=b.telefono_movil,
			a.direccion=b.direccion
		when not matched then insert (
			nombres,
			apellido_paterno,
			apellido_materno,
			tipo_documento,
			nro_documento,
			correo_electronico,
			sexo,
			telefono_fijo,
			telefono_movil,
			direccion
		) values (
			b.nombres,
			b.apellido_paterno,
			b.apellido_materno,
			b.tipo_documento,
			b.nro_documento,
			b.correo_electronico,
			b.sexo,
			b.telefono_fijo,
			b.telefono_movil,
			b.direccion
		);
		set @codigo = ( select @@identity )
		if(@codigo is null)
			set @codigo = (
				select x.v.value('codigo[1]', 'int') codigo
				from @params.nodes('/cliente/row') x(v)
			)

		merge cliente as a
		using (
			select 
				@codigo codigo_persona,
				x.v.value('tipo[1]', 'varchar(25)') tipo,
				x.v.value('categoria[1]', 'varchar(100)') categoria
			from @params.nodes('/cliente/row') x(v)
		) as b
		on a.codigo_persona=b.codigo_persona
		when matched then update set
			a.tipo=b.tipo,
			a.categoria=b.categoria
		when not matched then insert (
			codigo_persona,
			tipo,
			categoria
		) values (
			b.codigo_persona,
			b.tipo,
			b.categoria
		);

		if @tipo = 1
			merge cliente_natural as a
			using (
				select 
					@codigo codigo_cliente,
					x.v.value('nacionalidad[1]', 'varchar(50)') nacionalidad,
					convert(date, x.v.value('fecha_nacimiento[1]', 'varchar(25)'), 103) fecha_nacimiento
				from @params.nodes('/cliente/row') x(v)
			) as b
			on a.codigo_cliente=b.codigo_cliente
			when matched then update set
				a.nacionalidad=b.nacionalidad,
				a.fecha_nacimiento=b.fecha_nacimiento
			when not matched then insert (
				codigo_cliente,
				nacionalidad,
				fecha_nacimiento
			) values (
				b.codigo_cliente,
				b.nacionalidad,
				b.fecha_nacimiento
			);
		else if @tipo = 2
			merge cliente_juridico as a
			using (
				select 
					@codigo codigo_cliente,
					x.v.value('razon_social[1]', 'varchar(250)') razon_social,
					x.v.value('codigo_persona_contacto[1]', 'int') codigo_persona_contacto
				from @params.nodes('/cliente/row') x(v)
			) as b
			on a.codigo_cliente=b.codigo_cliente
			when matched then update set
				a.razon_social=b.razon_social,
				a.codigo_persona_contacto=b.codigo_persona_contacto
			when not matched then insert (
				codigo_cliente,
				razon_social,
				codigo_persona_contacto
			) values (
				b.codigo_cliente,
				b.razon_social,
				b.codigo_persona_contacto
			);

		commit transaction

		select @codigo codigo
	end try
	begin catch
		declare @error int, @message varchar(4000)
		select @error = error_number(), @message = error_message()
		
		if @@trancount >= 0
            rollback transaction;

		raiserror ('sp_crear_cliente: %d: %s', 16, 1, @error, @message) ;
	end catch
end
GO
/****** Object:  StoredProcedure [dbo].[usp_listar_age]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_listar_age]
as
begin
	select * from agencia order by nombre asc
end
GO
/****** Object:  StoredProcedure [dbo].[usp_listar_pers]    Script Date: 18/05/2019 10:09:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_listar_pers]
as
begin
	select * from persona order by nombres asc
end
GO
