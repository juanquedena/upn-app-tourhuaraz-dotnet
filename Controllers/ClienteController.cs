﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tour_huaraz.Models;
using tour_huaraz.Enums;
using tour_huaraz.Db.DAO;
using Microsoft.AspNetCore.Cors;

namespace tour_huaraz_api_v2.Controllers
{
    [Route("api/[controller]")]
    public class ClienteController : Controller
    {
        protected readonly ILogger _logger;
        
        public ClienteController(ILogger<ClienteController> logger)
        {
            _logger = logger;
        }

        // GET api/cliente
        [HttpGet]
        public IActionResult Get([FromQuery(Name = "search[value]")] string search, [FromQuery] int start, [FromQuery] int length, [FromQuery] int draw)
        {
            try
            {
                Object personas = ClienteDAO.List(search, start, length, draw);
                _logger.LogInformation($"Returned all clients from database.");
                return Ok(personas);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/cliente/{id}
        [HttpGet("{id}")]
		public IActionResult GetById(int id)
        {
            try
            {
                var persona = ClienteDAO.Obtain(id);

                if (persona == null)
                {
                    _logger.LogError($"Client with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInformation($"Returned client with id: {id}");
                    return Ok(persona);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        /*
        // DELETE api/cliente/{id}
        [HttpDelete("{id}")]
		public IActionResult DeleteById(int id)
        {
            try
            {
                var cliente = ClienteDAO.Obtain(id);
                if (cliente == null)
                {
                    _logger.LogError($"Client with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInformation($"Returned client with id: {id}");
                    ClienteDAO.Delete(id);
                    return  Accepted(cliente);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        */
    }
}
