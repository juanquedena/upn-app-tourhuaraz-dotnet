﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tour_huaraz.Models;
using tour_huaraz.Enums;
using tour_huaraz.Db.DAO;
using Microsoft.AspNetCore.Cors;

namespace tour_huaraz_api_v2.Controllers
{
    [Route("api/[controller]")]
    public class ClienteJuridicoController : Controller
    {
        protected readonly ILogger _logger;
        
        public ClienteJuridicoController(ILogger<ClienteJuridicoController> logger)
        {
            _logger = logger;
        }

        // POST api/cliente/juridico
        [HttpPost]
        public IActionResult CreateClient2([FromBody] ClienteJuridico clienteJuridico)
        {
            try
            {
                if (clienteJuridico == null)
                {
                    _logger.LogError("Cient object sent from client is null.");
                    return BadRequest("Client object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid client object sent from client.");
                    return StatusCode(409, ModelState.ToList());
                }

                clienteJuridico.Tipo = (int)TipoPersonaType.PERSONA_JURIDICA;
                ClienteDAO.SaveUpdate(clienteJuridico);

                return Created("api/cliente/{clienteJuridico.Codigo}", clienteJuridico);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Create action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/cliente/juridico
        [HttpPut("{id}")]
        public IActionResult PutClient2(int id, [FromBody] ClienteNatural clienteJuridico)
        {
            try
            {
                if (clienteJuridico == null)
                {
                    _logger.LogError("Cient object sent from client is null.");
                    return BadRequest("Client object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid client object sent from client.");
                    return StatusCode(409, ModelState.ToList());
                }

                var persona = ClienteDAO.Obtain(id);
                if (persona == null)
                {
                    _logger.LogError($"Client with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                clienteJuridico.Tipo = (int)TipoPersonaType.PERSONA_JURIDICA;
                ClienteDAO.SaveUpdate(clienteJuridico);

                return Created("api/cliente/{clienteJuridico.Codigo}", clienteJuridico);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Create action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
