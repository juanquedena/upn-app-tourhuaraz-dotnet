﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tour_huaraz.Models;
using tour_huaraz.Enums;
using tour_huaraz.Db.DAO;
using Microsoft.AspNetCore.Cors;

namespace tour_huaraz_api_v2.Controllers
{
    [Route("api/[controller]")]
    public class AgenciaController : Controller
    {
        protected readonly ILogger _logger;
        
        public AgenciaController(ILogger<AgenciaController> logger)
        {
            _logger = logger;
        }

        // GET api/cliente
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                Object agencias = AgenciaDAO.List();
                _logger.LogInformation($"Returned all agencias from database.");
                return Ok(agencias);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
