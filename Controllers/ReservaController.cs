﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tour_huaraz.Models;
using tour_huaraz.Enums;
using tour_huaraz.Db.DAO;
using Microsoft.AspNetCore.Cors;
using tour_huaraz.Db.Wrapper;

namespace tour_huaraz_api_v2.Controllers
{
    [Route("api/[controller]")]
    public class ReservaController : Controller
    {
        protected readonly ILogger _logger;
        
        public ReservaController(ILogger<ReservaController> logger)
        {
            _logger = logger;
        }

        // GET api/reserva
        [HttpGet]
        public IActionResult Get([FromQuery(Name = "search[value]")] string search, [FromQuery] int start, [FromQuery] int length, [FromQuery] int draw)
        {
            try
            {
                Object reservas = ReservaDAO.List(search, start, length, draw);
                _logger.LogInformation($"Returned all reservas from database.");
                return Ok(reservas);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside Get action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/reserva/{id}
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var reserva = ReservaDAO.Obtain(id);

                if (reserva == null)
                {
                    _logger.LogError($"Reserva with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInformation($"Returned reserva with id: {id}");
                    return Ok(reserva);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // POST api/reserva
        [HttpPost]
        public IActionResult CreateReserva([FromBody] Reserva reserva)
        {
            try
            {
                if (reserva == null)
                {
                    _logger.LogError("Reserva object sent from client is null.");
                    return BadRequest("Reserva object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid Reserva object sent from client.");
                    return StatusCode(409, ModelState.ToList());
                }

                ReservaDAO.Save(new ReservaWrapper(reserva));

                return Created("api/reserva/{reserva.Codigo}", reserva);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                _logger.LogError($"Something went wrong inside Create action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
