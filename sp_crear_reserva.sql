use tour_huaraz
go

drop procedure sp_crear_reserva;
go

create procedure sp_crear_reserva(
	@params xml
)
as
begin
	set nocount on;

	declare @codigo int

	begin transaction;

	begin try

		insert into reserva(codigo_cliente, fecha_reservacion, estado, codigo_agencia, subtotal, igv, total)
		select
			x.v.value('codigo_cliente[1]', 'int') codigo_cliente,
			convert(date, x.v.value('fecha_reservacion[1]', 'varchar(25)'), 103) fecha_reservacion,
			x.v.value('estado[1]', 'int') estado,
			x.v.value('codigo_agencia[1]', 'int') codigo_agencia,
			x.v.value('subtotal[1]', 'numeric(18, 3)') subtotal,
			x.v.value('igv[1]', 'numeric(18, 3)') igv,
			x.v.value('total[1]', 'numeric(18, 3)') total
		from @params.nodes('/reserva') x(v)
		set @codigo = ( select @@identity )

		insert into detalle_reserva(codigo_reserva, codigo_paquete, cantidad_a, cantidad_b, cantidad_c, precio_a, precio_b, precio_c)
		select
			@codigo codigo_reserva,
			x.v.value('codigo_paquete[1]', 'int') codgo_paquete,
			x.v.value('cantidad_a[1]', 'int') cantidad_a,
			x.v.value('cantidad_b[1]', 'int') cantidad_b,
			x.v.value('cantidad_c[1]', 'int') cantidad_c,
			x.v.value('precio_a[1]', 'numeric(18, 3)') precio_a,
			x.v.value('precio_b[1]', 'numeric(18, 3)') precio_b,
			x.v.value('precio_c[1]', 'numeric(18, 3)') precio_c
		from @params.nodes('/detalle_reserva/row') x(v)

		declare @clientes table(codigo int)
		declare @cliente xml
		declare cliente_cursor cursor for   
		select 
			x.v.query('.') cliente
		from @params.nodes('/turistas/cliente') x(v);
  
		open cliente_cursor
		fetch next from cliente_cursor into @cliente
  
		while @@fetch_status = 0
		begin
			insert into @clientes exec sp_crear_cliente 1, @cliente
			fetch next from cliente_cursor into @cliente
		end   
		close cliente_cursor;  
		deallocate cliente_cursor; 

		insert into turista (codigo_cliente_natural, codigo_reserva)
		select codigo, @codigo from @clientes

		commit transaction

		select @codigo codigo
	end try
	begin catch
		declare @error int, @message varchar(4000)
		select @error = error_number(), @message = error_message()
		
		if @@trancount >= 0
            rollback transaction;

		raiserror ('sp_crear_cliente: %d: %s', 16, 1, @error, @message) ;
	end catch
end
go

-- exec sp_crear_reserva '<cliente><row><codigo>0</codigo><nombres>jyq f</nombres><apellido_paterno>s</apellido_paterno><apellido_materno>s</apellido_materno><tipo_documento>1</tipo_documento><nro_documento>12345678</nro_documento><correo_electronico>d@d.com</correo_electronico><sexo>1</sexo><telefono_fijo>13</telefono_fijo><telefono_movil>3333</telefono_movil><direccion>d dsd sfsd</direccion></row></cliente>'